#!/usr/bin/env ruby

# A simple watchr to automate 
#
#     $ python setup install
#
# on file changes. This file should reside beside `setup.py`.
# Works best with https://github.com/mynyml/watchr 
watch('mdhub/.*\.py|mdhub/commands/.*\.py|bin/.*|')  { |md|
  makefile = File.join(File.dirname(__FILE__), 'Makefile')
  system("make -f #{makefile} clean")
  setuppy = File.join(File.dirname(__FILE__), 'setup.py')
  system("python #{setuppy} install")
}
