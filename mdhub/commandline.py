# -*- coding: utf-8 -*-
"""
Commandline module, command dispatch.
"""

import argparse
from mdhub import importconfig
from mdhub import statistics

import logging
import logging.config
logging.config.fileConfig('logging.conf')

def main():
    """ Command line entry point.
    """
    parser = argparse.ArgumentParser(
        description='Library metadata processing.')

    parser.add_argument('-c', '--config', metavar='FILENAME', type=unicode,
        nargs=1, help='Dump config file.')
    parser.add_argument('-d', '--dump', metavar='FILENAME', type=unicode,
        nargs=1, help='Dump binary MARC21 records from FILENAME to stdout.')
    parser.add_argument('-l', '--dump-leader', metavar='FILENAME',
        type=unicode, nargs=1, help='Dump binary MARC21 records from \
        FILENAME to stdout. Leader only.')
    parser.add_argument('-n', '--count', metavar='FILENAME', type=unicode,
        nargs='+', help='Count MARC21 records in FILENAME.')
    parser.add_argument('-u', '--tag-usage', metavar='FILENAME', type=unicode,
        nargs=1, help='Show tag usage for FILENAME containing multiple \
        MARC21 records.')
    parser.add_argument('-a', '--values', metavar='FILENAME', type=unicode,
        nargs=1, help='Show tag values and distribution for FILENAME \
        containing MARC21 records.')
    parser.add_argument('-t', '--tags', action='store_true',
        help='List marc tags.')
    parser.add_argument('-p', '--example-process', action='store_true',
        help='Run hardcoded example process.')

    args = parser.parse_args()

    if args.config:
        importconfig.FincImportConfiguration(args.config[0]).dump()
    elif args.values:
        statistics.dump_tag_values(args.values)
    elif args.dump:
        statistics.dump_marc(args.dump)
    elif args.tags:
        statistics.dump_tags()
    elif args.dump_leader:
        statistics.dump_marc_leader(args.dump_leader)
    elif args.count:
        statistics.dump_marc_record_count(args.count)
    elif args.tag_usage:
        statistics.dump_tag_usage(args.tag_usage)
    elif args.example_process:
        from mdhub import processor
        processor = processor.Processor(
            '~/bitbucket/miku/mdhub/config/v2/simple-fi.mc.xml',
            '~/bitbucket/miku/mdhub/config/v2/fincstore.cfg'
        )
        processor.process()

