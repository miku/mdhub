# -*- coding: utf-8 -*-

"""
Bacon.
"""

import ConfigParser
import time
import os
import copy
import logging
from mdhub import store
from mdhub import importconfig
from mdhub import location
from mdhub import exc
from mdhub import util

logger = logging.getLogger(__name__)

def get_store_type(store_conf_path):
    """
    For a path to store configuration file, get the
    kind of store we are dealing with.
    """
    parser = ConfigParser.SafeConfigParser()
    parser.read(os.path.expanduser(store_conf_path))
    return parser.sections()[0]

class Processor(object):
    """
    Hi, I'm god. Maybe you should refactor me.
    """
    def __init__(self, import_conf_path, store_conf_path):
        """
        Initialize the god processor. ``import_configuration`` and
        ``store_configuration`` are pathes to the configuration files,
        not objects.
        """
        # get a handle on a import configuration
        self.import_conf = importconfig.ImportConfiguration(import_conf_path)

        # Just get the store type out of the config
        # and set ``self.store`` accordingly (without import magic)
        store_type = get_store_type(store_conf_path)
        if store_type == 'redis':
            self.store = store.RedisStore(store_conf_path)
        elif store_type == 'rdbms':
            raise NotImplementedError
        else:
            raise exc.MetaDataHubException(
                "No suitable store defined in: {0}".format(
                    os.path.expanduser(store_conf_path)))
        # TODO: Add more store types here ...

    def process(self):
        """
        Given a FINC import configuration (``self.import_configuration``) and
        a store (key-value store, RDBMS) configuration
        (``self.store_configuration``) process all available records and
        feed them to our store appropriately.
        """
        start = time.time()
        logger.debug("Starting process...")
        records, commands = 0, 0
        iterator = location.record_iterator(self.import_conf.location)

        for record_iterator in iterator:
            for record in record_iterator:
                # Bootstrap internal representation of the metadata:
                # This dict will be passed along all commands; each command
                # is free to add and modify keys and values.
                bag = util.mdbag()
                bag['import_conf'] = self.import_conf
                bag['store'] = self.store
                bag['original'] = copy.deepcopy(record) # we store a python object at the moment

                logger.debug("Attempting to execute {0} commands on record".format(
                    len(self.import_conf.commands)))
                for command in self.import_conf.commands:
                    bag = command.execute(bag)
                    commands += 1

                # ``data`` is processed by now - should
                # be handed over to the export side of things
                records += 1

        stop = time.time()
        logger.info(
            "Processed {0} records and {1} commands in {2:.4f} seconds".format(
            records, commands, (stop - start)))
        logger.info("{0:.4f} records/second".format(
            (records / (stop - start))))
        logger.info("{0:.4f} commands/second".format(
            (commands / (stop - start))))

if __name__ == '__main__':
    processor = Processor(
        '~/bitbucket/miku/mdhub/config/v2/simple-fi.mc.xml',
        '~/bitbucket/miku/mdhub/config/v2/fincstore.cfg'
    )
    processor.process()

