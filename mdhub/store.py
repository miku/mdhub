# -*- coding: utf-8 -*-

"""
The gateway to our data(store).
"""

import redis
import ConfigParser
import pickle
import os

class RedisStore(object):
    """
    Redis store for debugging.
    """
    def __init__(self, configuration):
        super(RedisStore, self).__init__()
        self.config = ConfigParser.SafeConfigParser()
        self.config.read(os.path.expanduser(configuration))
        self.database = redis.Redis(
            host=self.config.get('redis', 'host'),
            port=int(self.config.get('redis', 'port')),
            db=int(self.config.get('redis', 'database')),
        )

    def get_record(self, finc_id):
        """
        Just return a plain record or None if ``finc_id`` isn't taken yet.
        """
        result = self.database.get(finc_id) # returns None on miss
        if not result == None:
            result = pickle.loads(result)
        return result

    def set_record(self, finc_id, payload):
        """
        Set record with finc_id as key. Payload will be *jsonified*
        by default.
        """
        self.database.set(finc_id, pickle.dumps(payload))

