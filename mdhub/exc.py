# -*- coding: utf-8 -*-
"""
Exceptions.
"""

class MetaDataHubException(Exception):
    """ Unspecified exception within mdhub.
    """
    pass
