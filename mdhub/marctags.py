# -*- coding: utf-8 -*

"""
MARC 21 Tag according to the spec.
"""

import re
from mdhub.exc import MetaDataHubException
from collections import OrderedDict

TAGS = OrderedDict((
    ('001', 'Control Number (NR)'),
    ('003', 'Control Number Identifier (NR)'),
    ('005', 'Date and Time of Latest Transaction (NR)'),
    ('006', 'Fixed-Length Data Elements - Additional Material \
Characteristics (R)'),
    ('007', 'Physical Description Fixed Field (R)'),
    ('008', 'Fixed-Length Data Elements (NR)'),
    ('009', 'Physical Description Fixed-Field for Archival Collection \
[obsolete] Currently reserved for local use.'),
    ('010', 'Library of Congress Control Number (NR)'),
    ('011', 'Linking Library of Congress Control Number [obsolete]'),
    ('013', 'Patent Control Information (R)'),
    ('015', 'National Bibliography Number (NR)'),
    ('016', 'National Bibliographic Agency Control Number (R)'),
    ('017', 'Copyright Registration Number (R)'),
    ('018', 'Copyright Article-Fee Code (NR)'),
    ('020', 'International Standard Book Number (R)'),
    ('022', 'International Standard Serial Number (R)'),
    ('023', 'Standard Film Number [deleted]'),
    ('024', 'Other Standard Identifier (R)'),
    ('025', 'Overseas Acquisition Number (R)'),
    ('027', 'Standard Technical Report Number (R)'),
    ('028', 'Publisher Number (R)'),
    ('030', 'CODEN Designation (R)'),
    ('032', 'Postal Registration Number (R)'),
    ('033', 'Date/Time and Place of an Event (R)'),
    ('034', 'Coded Cartographic Mathematical Data (R)'),
    ('035', 'System Control Number (R)'),
    ('036', 'Original Study Number for Computer Data Files (NR)'),
    ('037', 'Source of Acquisition (R)'),
    ('039', 'Level of Bibliographic Control and Coding Detail [obsolete]'),
    ('040', 'Cataloging Source (NR)'),
    ('041', 'Language Code (NR)'),
    ('042', 'Authentication Code (NR)'),
    ('043', 'Geographic Area Code (NR)'),
    ('044', 'Country of Publishing/Producing Entity Code (NR)'),
    ('045', 'Time Period of Content (NR)'),
    ('046', 'Special Coded Dates (NR)'),
    ('047', 'Form of Musical Composition Code (NR)'),
    ('048', 'Number of Musical Instruments or Voices Code (R)'),
    ('049', 'Local Holdings (NR)'),
    ('050', 'Library of Congress Call Number (R)'),
    ('051', 'Library of Congress Copy, Issue, Offprint Statement (R)'),
    ('052', 'Geographic Classification (R)'),
    ('055', 'Call Numbers/Class Numbers Assigned in Canada (R)'),
    ('060', 'National Library of Medicine Call Number (R)'),
    ('061', 'National Library of Medicine Copy Statement (R)'),
    ('066', 'Character Sets Present (NR)'),
    ('070', 'National Agricultural Library Call Number (R)'),
    ('071', 'National Agricultural Library Copy Statement (R)'),
    ('072', 'Subject Category Code (R)'),
    ('074', 'GPO Item Number (R)'),
    ('080', 'Universal Decimal Classification Number (R)'),
    ('082', 'Dewey Decimal Call Number (R)'),
    ('084', 'Other Call Number (R)'),
    ('086', 'Government Document Call Number (R)'),
    ('087', 'Report Number [obsolete] [CAN/MARC only]'),
    ('088', 'Report Number (R)'),
    ('090', 'Local Call Number'),
    ('091', 'Local Call Number'),
    ('092', 'Local Call Number'),
    ('093', 'Local Call Number'),
    ('094', 'Local Call Number'),
    ('095', 'Local Call Number'),
    ('096', 'Local Call Number'),
    ('097', 'Local Call Number'),
    ('098', 'Local Call Number'),
    ('099', 'Local Call Number'),
    ('100', 'Main Entry - Personal Name (NR)'),
    ('110', 'Main Entry - Corporate Name (NR)'),
    ('111', 'Main Entry - Meeting Name (NR)'),
    ('130', 'Main Entry - Uniform Title (NR)'),
    ('210', 'Abbreviated Title (R)'),
    ('211', 'Acronym or Shortened Title [obsolete]'),
    ('212', 'Variant Access Title [obsolete]'),
    ('214', 'Augmented Title [obsolete]'),
    ('222', 'Key Title (R)'),
    ('240', 'Uniform Title (NR)'),
    ('241', 'Romanized Title [obsolete]'),
    ('242', 'Translation of Title by Cataloging Agency (R)'),
    ('243', 'Collective Uniform Title (NR)'),
    ('245', 'Title Statement (NR)'),
    ('246', 'Varying Form of Title (R)'),
    ('247', 'Former Title or Title Variations (R)'),
    ('250', 'Edition Statement (NR)'),
    ('254', 'Musical Presentation Statement (NR)'),
    ('255', 'Cartographic Mathematical Data (R)'),
    ('256', 'Computer File Characteristics (NR)'),
    ('257', 'Country of Producing Entity for Archival Films (NR)'),
    ('260', 'Publication, Distribution, Etc. (Imprint) (NR)'),
    ('261', 'Imprint Statement for Films (Pre-AACR 1 Revised) (NR) \
(USA only )'),
    ('262', 'Imprint Statement for Sound Recordings (Pre-AACR 2) (NR) \
(USA only )'),
    ('263', 'Projected Publication Date (NR)'),
    ('265', 'Source for Acquisition/Subscription Address [obsolete]'),
    ('270', 'Address (R)'),
    ('300', 'Physical Description (R)'),
    ('301', 'Physical Description for Films (Pre-AACR 2) [obsolete]'),
    ('302', 'Page Count [obsolete]'),
    ('303', 'Unit Count [obsolete]'),
    ('304', 'Linear Footage [obsolete]'),
    ('305', 'Physical Description for Sound Recordings (Pre-AACR 2) \
[obsolete]'),
    ('306', 'Playing Time (NR)'),
    ('307', 'Hours, Etc. (R)'),
    ('308', 'Physical Description for Films (Archival) [obsolete]'),
    ('310', 'Current Publication Frequency (NR)'),
    ('315', 'Frequency [obsolete]'),
    ('321', 'Former Publication Frequency (R)'),
    ('340', 'Physical Medium (R)'),
    ('342', 'Geospatial Reference Data (R)'),
    ('343', 'Planar Coordinate Data (R)'),
    ('350', 'Price [obsolete]'),
    ('351', 'Organization and Arrangement of Materials (R)'),
    ('352', 'Digital Graphic Representation (R)'),
    ('355', 'Security Classification Control (R)'),
    ('357', 'Originator Dissemination Control (NR)'),
    ('359', 'Rental Price [obsolete]'),
    ('362', 'Dates of Publication and/or Sequential Designation (R)'),
    ('400', 'Series Statement/Added Entry - Personal Name (R) (USA only )'),
    ('410', 'Series Statement/Added Entry - Corporate Name (R) (USA only )'),
    ('411', 'Series Statement/Added Entry - Meeting Name (R) (USA only )'),
    ('440', 'Series Statement/Added Entry - Title (R)'),
    ('490', 'Series Statement (R)'),
    ('500', 'General Note (R)'),
    ('501', 'With Note (R)'),
    ('502', 'Dissertation Note (R)'),
    ('503', 'Bibliographic History Note [obsolete]'),
    ('504', 'Bibliography, Etc. Note (R)'),
    ('505', 'Formatted Contents Note (R)'),
    ('506', 'Restrictions on Access Note (R)'),
    ('507', 'Scale Note for Graphic Material (NR)'),
    ('508', 'Creation/Production Credits Note (NR)'),
    ('510', 'Citation/References Note (R)'),
    ('511', 'Participant or Performer Note (R)'),
    ('512', 'Earlier or Later Volumes Separately Cataloged Note [obsolete]'),
    ('513', 'Type of Report and Period Covered Note (R)'),
    ('514', 'Data Quality Note (NR)'),
    ('515', 'Numbering Peculiarities Note (R)'),
    ('516', 'Type of Computer File or Data Note (R)'),
    ('517', 'Categories of Films Note (Archival) [obsolete]'),
    ('518', 'Date/Time and Place of an Event Note (R)'),
    ('520', 'Summary, Etc. (R)'),
    ('521', 'Target Audience Note (R)'),
    ('522', 'Geographic Coverage Note (R)'),
    ('523', 'Time Period of Content Note [obsolete]'),
    ('524', 'Preferred Citation of Described Materials Note (R)'),
    ('525', 'Supplement Note (R)'),
    ('526', 'Study Program Information Note (R)'),
    ('527', 'Censorship Note [obsolete]'),
    ('530', 'Additional Physical Form Available Note (R)'),
    ('533', 'Reproduction Note (R)'),
    ('534', 'Original Version Note (R)'),
    ('535', 'Location of Originals/Duplicates Note (R)'),
    ('536', 'Funding Information Note (R)'),
    ('537', 'Source of Data Note [obsolete]'),
    ('538', 'System Details Note (R)'),
    ('540', 'Terms Governing Use and Reproduction Note (R)'),
    ('541', 'Immediate Source of Acquisition Note (R)'),
    ('543', 'Solicitation Information Note [obsolete]'),
    ('544', 'Location of Other Archival Materials Note (R)'),
    ('545', 'Biographical or Historical Data (R)'),
    ('546', 'Language Note (R)'),
    ('547', 'Former Title Complexity Note (R)'),
    ('550', 'Issuing Body Note (R)'),
    ('552', 'Entity and Attribute Information Note (R)'),
    ('555', 'Cumulative Index/Finding Aids Note (R)'),
    ('556', 'Information About Documentation Note (R)'),
    ('561', 'Ownership and Custodial History (R)'),
    ('562', 'Copy and Version Identification Note (R)'),
    ('565', 'Case File Characteristics Note (R)'),
    ('567', 'Methodology Note (R)'),
    ('570', 'Editor Note [obsolete]'),
    ('580', 'Linking Entry Complexity Note (R)'),
    ('581', 'Publications About Described Materials Note (R)'),
    ('582', 'Related Computer Files Note [obsolete]'),
    ('583', 'Action Note (R)'),
    ('584', 'Accumulation and Frequency of Use Note (R)'),
    ('585', 'Exhibitions Note (R)'),
    ('586', 'Awards Note (R)'),
    ('590', 'Local Notes (R)'),
    ('591', 'Local Notes (R)'),
    ('592', 'Local Notes (R)'),
    ('593', 'Local Notes (R)'),
    ('594', 'Local Notes (R)'),
    ('595', 'Local Notes (R)'),
    ('596', 'Local Notes (R)'),
    ('597', 'Local Notes (R)'),
    ('598', 'Local Notes (R)'),
    ('599', 'Local Notes (R)'),
    ('600', 'Subject Added Entry - Personal Name (R)'),
    ('610', 'Subject Added Entry - Corporate Name (R)'),
    ('611', 'Subject Added Entry - Meeting Name (R)'),
    ('630', 'Subject Added Entry - Uniform Title (R)'),
    ('650', 'Subject Added Entry - Topical Term (R)'),
    ('651', 'Subject Added Entry - Geographic Name (R)'),
    ('652', 'Subject Added Entry - Reversed Geographic [obsolete]'),
    ('653', 'Index Term - Uncontrolled (R)'),
    ('654', 'Subject Added Entry - Faceted Topical Terms (R)'),
    ('655', 'Index Term - Genre/Form (R)'),
    ('656', 'Index Term - Occupation (R)'),
    ('657', 'Index Term - Function (R)'),
    ('658', 'Index Term - Curriculum Objective (R)'),
    ('680', 'PRECIS Descriptor String [obsolete, 1991] [CAN/MARC only]'),
    ('681', 'PRECIS Subject Indicator Number (SIN) [obsolete, 1991] \
[CAN/MARC only]'),
    ('683', 'PRECIS Reference Indicator Number (RIN) [obsolete, 1991] \
[CAN/MARC only]'),
    ('690', 'Local Subject Access Fields (R)'),
    ('691', 'Local Subject Access Fields (R)'),
    ('692', 'Local Subject Access Fields (R)'),
    ('693', 'Local Subject Access Fields (R)'),
    ('694', 'Local Subject Access Fields (R)'),
    ('695', 'Local Subject Access Fields (R)'),
    ('696', 'Local Subject Access Fields (R)'),
    ('697', 'Local Subject Access Fields (R)'),
    ('698', 'Local Subject Access Fields (R)'),
    ('699', 'Local Subject Access Fields (R)'),
    ('700', 'Added Entry - Personal Name (R)'),
    ('705', 'Added Entry - Personal Name (Performer) [obsolete]'),
    ('710', 'Added Entry - Corporate Name (R)'),
    ('711', 'Added Entry - Meeting Name (R)'),
    ('715', 'Added Entry - Corporate Name (Performing Group) [obsolete]'),
    ('720', 'Added Entry - Uncontrolled Name (R)'),
    ('730', 'Added Entry - Uniform Title (R)'),
    ('740', 'Added Entry - Uncontrolled Related/Analytical Title (R)'),
    ('751', 'Geographic Name/Area Name Entry [obsolete] [CAN/MARC only]'),
    ('752', 'Added Entry - Hierarchical Place Name (R)'),
    ('753', 'System Details Access to Computer Files (R)'),
    ('754', 'Added Entry - Taxonomic Identification (R)'),
    ('755', 'Added Entry - Physical Characteristics [obsolete]'),
    ('760', 'Main Series Entry (R)'),
    ('762', 'Subseries Entry (R)'),
    ('765', 'Original Language Entry (R)'),
    ('767', 'Translation Entry (R)'),
    ('770', 'Supplement/Special Issue Entry (R)'),
    ('772', 'Parent Record Entry (R)'),
    ('773', 'Host Item Entry (R)'),
    ('774', 'Constituent Unit Entry (R)'),
    ('775', 'Other Edition Entry (R)'),
    ('776', 'Additional Physical Form Entry (R)'),
    ('777', 'Issued With Entry (R)'),
    ('780', 'Preceding Entry (R)'),
    ('785', 'Succeeding Entry (R)'),
    ('786', 'Data Source Entry (R)'),
    ('787', 'Nonspecific Relationship Entry (R)'),
    ('800', 'Series Added Entry - Personal Name (R)'),
    ('810', 'Series Added Entry - Corporate Name (R)'),
    ('811', 'Series Added Entry - Meeting Name (R)'),
    ('830', 'Series Added Entry - Uniform Title (R)'),
    ('840', 'Series Added Entry - Title [obsolete]'),
    ('841', 'Holdings Coded Data Values (NR) Holdings Data Format'),
    ('842', 'Textual Physical Form Designator (NR) Holdings Data Format'),
    ('843', 'Reproduction Note (R) Holdings Data Format'),
    ('844', 'Name of Unit (NR) Holdings Data Format'),
    ('845', 'Terms Governing Use and Reproduction Note (R) \
Holdings Data Format'),
    ('850', 'Holding Institution (R)'),
    ('851', 'Location [obsolete]'),
    ('852', 'Location (R)'),
    ('853', 'Captions and Pattern - Basic Bibliographic Unit (R) \
Holdings Data Format'),
    ('854', 'Captions and Pattern - Supplementary Material (R) \
Holdings Data Format'),
    ('855', 'Captions and Pattern - Indexes (R) Holdings Data Format'),
    ('856', 'Electronic Location and Access (R)'),
    ('863', 'Enumeration and Chronology - Basic Bibliographic Unit (R) \
Holdings Data Format'),
    ('864', 'Enumeration and Chronology - Supplementary Material (R) \
Holdings Data Format'),
    ('865', 'Enumeration and Chronology - Indexes (R) Holdings Data Format'),
    ('866', 'Textual Holdings - Basic Bibliographic Unit (R) \
Holdings Data Format'),
    ('867', 'Textual Holdings - Supplementary Material (R) \
Holdings Data Format'),
    ('868', 'Textual Holdings - Indexes (R) Holdings Data Format'),
    ('870', 'Variant Personal Name [obsolete]'),
    ('871', 'Variant Corporate Name [obsolete]'),
    ('872', 'Variant Conference or Meeting Name [obsolete]'),
    ('873', 'Variant Uniform Title Heading [obsolete]'),
    ('876', 'Item Information - Basic Bibliographic Unit (R) \
Holdings Data Format'),
    ('877', 'Item Information - Supplementary Material (R) \
Holdings Data Format'),
    ('878', 'Item Information - Indexes (R) Holdings Data Format'),
    ('880', 'Alternate Graphic Representation (R)'),
    ('886', 'Foreign MARC Information Field (R)'),
    ('9XX', 'Locally-Defined Fields'),
    ('9XX', 'Equivalence and Cross-Reference Fields (Canada only)'),
))

def describe(number):
    """ Describe the tag given by number. Parameter will be converted to
    a string and will only return something useful, if the parameter isn't
    cluttered with anything else.

    >>> describe("001s")
    Traceback (most recent call last):
        'Parameter should contain numbers only, got: {0}'.format(number))
    MetaDataHubException: Parameter should contain numbers only, got: 001s
    >>> describe("001")
    'Control Number (NR)'
    >>> describe("100")
    'Main Entry - Personal Name (NR) [Main Entry]'
    >>> describe(100)
    'Main Entry - Personal Name (NR) [Main Entry]'
    """
    try:
        number = "%03d" % int(number)
    except ValueError:
        raise MetaDataHubException(
        'Parameter should contain numbers only, got: {0}'.format(number))

    if not re.match('^\d\d\d$', number):
        raise MetaDataHubException(
        'Outside formal number range (000-999): {0}'.format(number))

    category = None

    if re.match('^\d[1-9]\d$', number):
        category = 'Number and Code'
    elif re.match('^1\d\d$', number):
        category = 'Main Entry'
    elif re.match('^2[0-4]\d$', number):
        category = 'Title'
    elif re.match('^2[5-7]\d$', number):
        category = 'Edition, Imprint, etc'
    elif re.match('^3\d\d$', number):
        category = 'Physical Description, etc'
    elif re.match('^4\d\d$', number):
        category = 'Series Statement'
    elif re.match('^5\d\d$', number):
        category = 'Note'
    elif re.match('^6\d\d$', number):
        category = 'Subject Access'
    elif re.match('^7[0-5]\d$', number):
        category = 'Add entry'
    elif re.match('^7[6-9]\d$', number):
        category = 'Linking entry'
    elif re.match('^8[0-3]\d|840$', number):
        category = 'Series added entry'
    elif re.match('^84[1-9]|8[5-9]\d$', number):
        category = 'Holding, Alternate Graphic, etc'
    elif re.match('^9\d\d$', number):
        category = 'Locally Defined Field'

    try:
        message = TAGS[number]
        if not category == None:
            message += ' [{0}]'.format(category)
    except KeyError:
        message = 'unassigned'

    return message

if __name__ == '__main__':
    import doctest
    doctest.testmod()


