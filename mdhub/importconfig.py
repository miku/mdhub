# -*- coding: utf-8 -*-

"""
FINC import configuration.
"""

from lxml import objectify
import os
from mdhub import commands as commandsmodule

class ImportConfiguration(object):
    """
    Configuration wrapper around our import XML files.
    """
    def __init__(self, filename):
        self.filename = os.path.expanduser(filename)
        self._root_node = None
        self._datasource_attributes = None
        self._commands = []

    def __getattr__(self, attr):
        if attr in self.datasource_attributes:
            return self.datasource_attributes[attr]
        raise AttributeError

    @property
    def root_node(self):
        """
        XML root node of the configuration.
        """
        if self._root_node == None:
            with open(self.filename, 'r') as handle:
                self._root_node = objectify.parse(handle).getroot()
        return self._root_node

    @property
    def datasource_attributes(self):
        """ Datasource attributes. These attributes are 'global'.
        """
        if self._datasource_attributes == None:
            self._datasource_attributes = dict(self.root_node[0].items())
        return self._datasource_attributes

    @property
    def commands(self):
        """
        These commands are executed *per record*
        """
        if self._commands == []:
            for node in self.root_node[0].process[0].iterchildren():
                # TODO We haven't found an ignore_comments or similar
                # attribute for the ``objective.parse`` function; so we
                # need to filter comment nodes out manually here.
                if node.tag == 'comment':
                    continue
                self._commands.append(
                    commandsmodule.command_factory(node.tag, **dict(node.items())))
        return self._commands

    def dump(self):
        print "Configuration from: {0}".format(self.filename)
        print "Datasource spec: {0}".format(self.datasource_attributes)
        print "Commands: {0}".format(self.commands)

if __name__ == '__main__':
    import_conf = ImportConfiguration('~/bitbucket/miku/mdhub/config/v2/simple-fi.mc.xml')
    print import_conf.datasource_attributes['type']
    print import_conf.datasource_attributes
    print import_conf.commands

