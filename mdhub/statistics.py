# -*- coding: utf-8 -*-

"""
Some simple statistics on MARC files.
"""

import os
import sys
import collections
import pymarc
import itertools
from mdhub import marctags

def dump_tag_values(list_of_files):
    """
    Display tag values for each file in ``list_of_files``
    """
    tag_values = collections.defaultdict(collections.Counter)
    records = 0
    for filename in list_of_files:
        if not os.path.exists(filename):
            raise Exception("No such file: {0}".format(filename))
        reader = pymarc.MARCReader(file(filename))
        for record in reader:
            records += 1
            for field in record:
                tag_values[field.tag]['{0}'.format(field)] += 1
    for tag, values in sorted(tag_values.items()):
        print '{0} {1} ({2} uniq in {3} records) ==>'.format(
            tag, marctags.describe(tag), len(values), records)
        for content, count in values.most_common():
            print "{0: 8} {1}".format(count, content)

def dump_marc(list_of_files):
    """
    Similar to yaz-marcdump.
    """
    for filename in list_of_files:
        if not os.path.exists(filename):
            raise Exception("No such file: {0}".format(filename))
        reader = pymarc.MARCReader(file(filename))
        for i, record in enumerate(reader):
            print '{0: 10} ==> Leader ==> {1}'.format(i, record.leader)
            for j, field in enumerate(record):
                print '{0: 10}{1: 5} ==> {2}'.format(i, j, field.tag),
                if hasattr(field, 'subfields'):
                    print 'I:', field.indicators, 'S:', \
                        dict(itertools.izip_longest(
                            *[iter(field.subfields)] * 2, fillvalue=''))
                else:
                    print field.data # , field.__dict__
            print '----------'

def dump_tags():
    """
    Just list MARC tag human readable exaplanation.
    """
    for key, value in marctags.TAGS.items():
        print "{0: <10} {1}".format(key, value)

def dump_marc_leader(list_of_files):
    """
    Just dump the MARC leaders.
    """
    for filename in list_of_files:
        if not os.path.exists(filename):
            raise Exception("No such file: {0}".format(filename))
        reader = pymarc.MARCReader(file(filename))
        for i, record in enumerate(reader):
            print i, record.leader

def dump_marc_record_count(list_of_files):
    """
    Display number of MARC records in each file.
    """
    for filename in list_of_files:
        if not os.path.exists:
            print >> sys.stderr, 'No such file: {0}'.format(filename)
        else:
            reader = pymarc.MARCReader(file(filename))
            print '{0}: {1}'.format(filename, len([ r for r in reader ]))

def dump_tag_usage(list_of_files):
    """
    Display tags by usage frequency per file.
    """
    tags = collections.Counter()
    records = 0
    for filename in list_of_files:
        if not os.path.exists(filename):
            raise Exception("No such file: {0}".format(filename))
        reader = pymarc.MARCReader(file(filename))
        for record in reader:
            records += 1
            for field in record:
                tags[field.tag] += 1
    for tag, count in tags.most_common():
        print '{0}\t{1: 10} ({2:.2%})\t{3}'.format(
            tag, count, (float(count)/records), marctags.describe(tag))

