# -*- coding: utf-8 -*-

"""
Commands/Transformations implementation.
"""

import copy
import pymarc
from mdhub import util
import logging

logger = logging.getLogger(__name__)
COMMAND_MAP = {} # forward declaration

def command_factory(name, **kwargs):
    """
    Lookup and create the appropriate command object.
    All available commands should be listed in ``COMMAND_MAP``.
    """
    command_object = COMMAND_MAP[name](name, **kwargs)
    return command_object

class Command(object):
    """
    Command superclass.
    """
    def __init__(self, name, **kwargs):
        self.name = name
        self.argmap = kwargs

    def __repr__(self):
        return u'<{0} {1}>'.format(self.name, self.argmap.items())

    def execute(self, bag):
        """
        Stub implementation of execute()
        """
        logger.debug('{0} {1}'.format(
            self.__class__.__name__, self.argmap))
        return bag

class Noop(Command):
    """
    An example noop command. Just some kind of identity function.
    """
    pass

class Copyall(Command):
    """
    Copy all tags/field into our own MARC record.
    """
    def execute(self, bag):
        super(Copyall, self).execute(bag)
        if bag['type'] == 'marc':
            bag['marc'] = copy.deepcopy(bag['original'])
        else:
            logger.error("Don't know how to copyall on non-MARC input.")
            raise NotImplementedError
        return bag

class Copy(Command):
    """
    Copy a ``src`` field value into a ``dst`` field value.
    """
    def execute(self, bag):
        super(Copy, self).execute(bag)
        if bag['type'] == 'marc':
            src, dst = self.argmap['src'], self.argmap['dst']
            fields = copy.deepcopy(bag['original'].get_fields(src))
            for field in fields:
                field.tag = dst
            bag['marc'].add_field(fields)
        else:
            logger.error(
                "Don't know how to map non-MARC field to MARC fields yet.")
            raise NotImplementedError
        return bag

class GetFincID(Command):
    """
    Get a finc id. Either a FINC ID identified by source and record id is
    already in our DB or it isn't.
    """
    def execute(self, bag):
        super(GetFincID, self).execute(bag)
        store = bag['store']
        if bag['type'] == 'marc':
            finc_id = util.encode_fincid(bag.get_original_marc_value('001'),
                source_id=bag['source_id'])
            record = store.get_record(finc_id)
            if record == None:
                store.set_record(finc_id, 'dummy')
            bag['finc_id'] = finc_id

COMMAND_MAP = {
    'noop' : Noop,
    'copyall' : Copyall,
    'copy' : Copy,
    'get_finc_id' : GetFincID,
}

