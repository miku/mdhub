# -*- coding: utf-8 -*-

from mdhub import location
import unittest

class TestLocation(unittest.TestCase):
    """
    Test location iteration functions. This is very incomplete.
    """
    def test_record_iterator(self):
        """
        Do we return really records?
        """
        iterator = location.record_iterator('.')
        self.assertTrue(getattr(iterator, '__iter__', False))

    def test_get_cache_dir(self):
        """
        Cache dir stuff.
        """
        import hashlib
        sha1 = hashlib.sha1()
        sha1.update('example')
        expected = sha1.hexdigest()
        self.assertTrue(location.get_cache_dir('example').endswith(expected))

