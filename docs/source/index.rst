.. mdhub documentation master file, created by
   sphinx-quickstart on Tue Oct 25 20:31:23 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

mdhub -- finc metadata management
================================


.. toctree::
   :maxdepth: 2

   intro
   glossary

.. Indices and tables
.. ==================
.. 
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
.. 
