#!/usr/bin/env python

from distutils.core import setup
import setuptools # so you can python setup.py develop

setup(
    name='mdhub',
    version='0.1',
    description='Library metadata management application.',
    author='Martin Czygan',
    author_email='martin.czygan@uni-leipzig.de',
    packages=['mdhub'],
    scripts=['bin/mdhub', 'bin/fincencode', 'bin/fincdecode'],
    # cmdclass={'build_manpage': build_manpage}
)

